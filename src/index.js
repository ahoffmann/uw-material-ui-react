import { createMuiTheme } from "@material-ui/core/styles";

import { colorRed, colorBlue, colorBlackFont, colorGrayLight } from "./colors";
import ButtonOverrides from "./overrides/buttons";

export const themeConfig = {
  props: {
    MuiButton: {
      color: "secondary",
    },
    MuiLink: {
      color: "secondary",
    },
  },
  overrides: {
    MuiButton: ButtonOverrides,
    MuiFormLabel: {
      root: {
        "&$focused": {
          color: colorBlue,
        },
      },
    },
    MuiOutlinedInput: {
      root: {
        "&:hover $notchedOutline": {
          borderColor: colorBlue,
        },
        "@media (hover: none)": {
          "&:hover $notchedOutline": {
            borderColor: colorBlue,
          },
        },
        "&$focused $notchedOutline": {
          borderColor: colorBlue,
        },
      },
    },
    MuiListSubheader: {
      root: {
        color: colorGrayLight,
        fontWeight: "bolder",
      },
    },
  },
  palette: {
    type: "light",
    primary: {
      main: colorRed,
    },
    secondary: {
      main: colorBlue,
    },
    text: {
      primary: colorBlackFont,
    },
  },
  shape: {
    borderRadius: 2,
  },
};

export const theme = createMuiTheme(themeConfig);
