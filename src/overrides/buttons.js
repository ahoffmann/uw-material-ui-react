import defaultTheme from "../defaultTheme";
import { colorBlueHover, colorGrayHover, colorGrayLight } from "../colors";
import {fade} from '@material-ui/core/styles/colorManipulator';


const containedOverrides = {
  "&:hover": {
    backgroundColor: colorBlueHover,
  },
  "&:disabled": {
    borderColor: colorGrayLight,
    color: colorGrayLight,
    boxShadow: defaultTheme.shadows[2],
  },
};

const outlinedOverrides = {
  borderColor: colorGrayLight,
  "&:hover": {
    borderColor: colorGrayLight,
    backgroundColor: colorGrayHover,
  },
  "&:disabled": {
    borderColor: colorGrayLight,
    color: colorGrayLight,
  },
};

const textOverrides = {
  "&:hover": {
    backgroundColor: fade(defaultTheme.palette.action.active, defaultTheme.palette.action.hoverOpacity)
  },
  "&:disabled": {
    color: colorGrayLight,
  },
};

export default {
  /** Styles applied when `variant="contained"` */
  contained: containedOverrides,
  /** Styles applied when `variant="contained"` and `color="secondary"` */
  containedSecondary: containedOverrides,
  /** Styles applied when `variant="outlined"` */
  outlined: outlinedOverrides,
  /** Styles applied when `variant="outlined"` and `color="secondary"` */
  outlinedSecondary: outlinedOverrides,
  /** Styles applied when `variant="text"` */
  text: textOverrides,
  /** Styles applied when `variant="text"` and `color="secondary"` */
  textSecondary: textOverrides,
};
